import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Rain {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		Scanner scanner;

		scanner = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
//		 scanner = new Scanner(new File("A-large-practice.in"));
//		 scanner = new Scanner(new File("B-small-practice.in"));
		int numCases = scanner.nextInt();
		for (int i = 1; i <= numCases; ++i) {

			int numRow = scanner.nextInt();
			int numCol = scanner.nextInt();
			// System.out.println(numOfPeople);
			scanner.nextLine();
			int island[][] = new int[numRow][numCol];
			for (int j = 0; j < numRow; j++) {
				for (int k = 0; k < numCol; k++) {
					island[j][k] = scanner.nextInt();
				}
			}

			System.out.printf("Case #%d: %d\n",i,rain(island, numRow, numCol));

		}

	}

	static boolean[][] visited = null;
	static int currentIsland[][]=null;
	static int totalRain=0;
	private static int rain(int[][] island, int numRow, int numCol) {
		totalRain=0;
		// TODO Auto-generated method stub
		currentIsland = new int[numRow][numCol];
		for (int i = 0; i < numRow ; i++) {
			for (int j = 0; j < numCol ; j++) {
				currentIsland[i][j] = island[i][j];
			}
		}
		
		for(int k=0;k<1000;k++){
			for (int i = 1; i < numRow - 1; i++) {
				for (int j = 1; j < numCol - 1; j++) {
					visited = new boolean[numRow][numCol];
	//				System.out.println("process: " + i + " " + j + "=" + island[i][j]);
	
					resetVisited();
					int maxValue = rainRecursivlyGetIncreaseValue(island, numRow, numCol, i, j);
	
	//				System.out.println(maxValue);
	
					
					resetVisited();
					rainRecursivlyAccumulate(island, numRow, numCol, i, j, maxValue);
					displayCurrentIsland( numRow, numCol);
					
					
				}
			}
		}
		return totalRain;
	}
	
	

	private static void resetVisited() {
		// TODO Auto-generated method stub
		for (int i = 0; i < visited.length ; i++) {
			for (int j = 0; j < visited[0].length ; j++) {
				visited[i][j] =false;
			}
		}
	}



	private static void displayCurrentIsland( int numRow, int numCol) {
		// TODO Auto-generated method stub
		System.out.println("===========displayCurrentIsland");
		for (int i =0 ; i < numRow; i++) {
			for (int j = 0; j < numCol ; j++) {
				System.out.print(currentIsland[i][j]+" ");
			}
			System.out.println();
		}
		
		System.out.println();
	}

	private static int rainRecursivlyAccumulate(int[][] island, int numRow, int numCol, int i, int j, int maxValue) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
//		System.out.println("===========rainRecursivlyAccumulate");
		if (visited[i][j]) {// base case edge/visited
			visited[i][j] = true;
			// System.out.println("\tvisited");
			return island[i][j];
		}

		if (i == 0 || i == numRow - 1) {
			visited[i][j] = true;
			// System.out.println("\tedge");
			return island[i][j];
		}

		if (j == 0 || j == numCol - 1) {
			visited[i][j] = true;
			// System.out.println("\tedge");
			return island[i][j];
		}

		visited[i][j] = true;

		ArrayList<Integer> adjacentValue = new ArrayList();
		// System.out.println("up");

		/*
		 * up higher? not go down higher? not go left higher? not go right
		 * higher? not go
		 * 
		 */

		int upValue = island[i - 1][j];
		if (!visited[i - 1][j]) {
			if (upValue - island[i][j] > 0) {
				adjacentValue.add(upValue);
			} else {
				adjacentValue.add(rainRecursivlyAccumulate(island, numRow, numCol, i - 1, j,maxValue));// up
			}
		}
		// System.out.println("down");
		int downValue = island[i + 1][j];
		if (!visited[i + 1][j]) {
			if (downValue - island[i][j] > 0) {
				adjacentValue.add(downValue);
			} else {
				adjacentValue.add(rainRecursivlyAccumulate(island, numRow, numCol, i + 1, j,maxValue));// down
			}
		}
		// System.out.println("left");

		int leftValue = island[i][j - 1];
		if (!visited[i][j - 1]) {
			if (leftValue - island[i][j] > 0) {
				adjacentValue.add(leftValue);
			} else {
				adjacentValue.add(rainRecursivlyAccumulate(island, numRow, numCol, i, j - 1,maxValue));// left
			}
		}
		// System.out.println("right");

		int rightValue = island[i][j + 1];
		if (!visited[i][j + 1]) {
			if (rightValue - island[i][j] > 0) {
				adjacentValue.add(rightValue);
			} else {
				adjacentValue.add(rainRecursivlyAccumulate(island, numRow, numCol, i, j + 1,maxValue));// right
			}
		}

		for (int value : adjacentValue) {
//			System.out.print(value + " ");
		}
//		System.out.println();

		int min = MAX;
		for (int value : adjacentValue) {

			if (min > value && value > 0 && value < MAX / 2) {
				min = value;
			}
		}

		if(maxValue>currentIsland[i][j]){
//			int increasedValue = maxValue - island[i][j];
			currentIsland[i][j]=maxValue;
			totalRain+=maxValue-island[i][j];
		}
//		System.out.printf("currentIsland[%d][%d]=%d",i,j,maxValue);
		
		if (min > 0 && min < MAX / 2) {// -ve return 0;
			return min;

		} else {
			return 0;
		}

	}

	static int MAX = 1000000000;

	private static int rainRecursivlyGetIncreaseValue(int[][] island, int numRow, int numCol, int i, int j) {
		// TODO Auto-generated method stub
		if (visited[i][j]) {// base case edge/visited
			visited[i][j] = true;
			// System.out.println("\tvisited");
			return island[i][j];
		}

		if (i == 0 || i == numRow - 1) {
			visited[i][j] = true;
			// System.out.println("\tedge");
			return island[i][j];
		}

		if (j == 0 || j == numCol - 1) {
			visited[i][j] = true;
			// System.out.println("\tedge");
			return island[i][j];
		}

		visited[i][j] = true;

		ArrayList<Integer> adjacentValue = new ArrayList();
		// System.out.println("up");

		/*
		 * up higher? not go down higher? not go left higher? not go right
		 * higher? not go
		 * 
		 */

		int upValue = island[i - 1][j];
		if (!visited[i - 1][j]) {
			if (upValue - island[i][j] > 0) {
				adjacentValue.add(upValue);
			} else {
				adjacentValue.add(rainRecursivlyGetIncreaseValue(island, numRow, numCol, i - 1, j));// up
			}
		}
		// System.out.println("down");
		int downValue = island[i + 1][j];
		if (!visited[i + 1][j]) {
			if (downValue - island[i][j] > 0) {
				adjacentValue.add(downValue);
			} else {
				adjacentValue.add(rainRecursivlyGetIncreaseValue(island, numRow, numCol, i + 1, j));// down
			}
		}
		// System.out.println("left");

		int leftValue = island[i][j - 1];
		if (!visited[i][j - 1]) {
			if (leftValue - island[i][j] > 0) {
				adjacentValue.add(leftValue);
			} else {
				adjacentValue.add(rainRecursivlyGetIncreaseValue(island, numRow, numCol, i, j - 1));// left
			}
		}
		// System.out.println("right");

		int rightValue = island[i][j + 1];
		if (!visited[i][j + 1]) {
			if (rightValue - island[i][j] > 0) {
				adjacentValue.add(rightValue);
			} else {
				adjacentValue.add(rainRecursivlyGetIncreaseValue(island, numRow, numCol, i, j + 1));// right
			}
		}

		for (int value : adjacentValue) {
//			System.out.print(value + " ");
		}
//		System.out.println();

		int min = MAX;
		for (int value : adjacentValue) {

			if (min > value && value > 0 && value < MAX / 2) {
				min = value;
			}
		}

		if (min > 0 && min < MAX / 2) {// -ve return 0;
			return min;

		} else {
			return 0;
		}

	}

}
