import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class CountryLeader {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		Scanner scanner = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
		 Scanner scanner;
		 try {
//			 scanner = new Scanner(new File("A-large-practice.in"));
		 scanner = new Scanner(new File("A-small-practice.in"));
//		 scanner = new Scanner(new File("Input.txt"));

		int numCases = scanner.nextInt();
		for (int i = 1; i <= numCases; ++i) {
			int numOfPeople = scanner.nextInt();
			// System.out.println(numOfPeople);
			scanner.nextLine();
			ArrayList<String> allPeopleThisCase = new ArrayList();
			for (int j = 0; j < numOfPeople; j++) {
				// System.out.println("j:"+j);
				String currentPeopleWholeName = scanner.nextLine();
				// System.out.println(currentPeopleWholeName);
				allPeopleThisCase.add(currentPeopleWholeName);

			}

			
			 Collections.sort(allPeopleThisCase);
			String chosenPerson = elect(allPeopleThisCase);

			System.out.println("Case #" + i + ": " + chosenPerson);
		}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static String elect(ArrayList<String> allPeopleThisCase) {
		// TODO Auto-generated method stub
		String maxPerson = "";
		int max = 0;
		for (String people : allPeopleThisCase) {
			int countOfHim = countAlphabet(people);
			if (max < countOfHim) {
				maxPerson = people;
				max = countOfHim;
			}
		}
		
		for (String people : allPeopleThisCase) {
			int countOfHim = countAlphabet(people);
			if (max==countOfHim ){
				return people;
			}
		}
		

		return maxPerson;
	}

	private static int countAlphabet(String people) {
		// TODO Auto-generated method stub
		int count = 0;
		Map map=new TreeMap();
		for (int i = 0; i < people.length(); i++) {
			
			if (people.charAt(i) >= 'A' && people.charAt(i) <= 'Z') {
				map.put(people.charAt(i), people.charAt(i));
			}
			
		}
		return map.size();
	}

}
