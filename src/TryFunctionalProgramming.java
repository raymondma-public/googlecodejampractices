import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

public class TryFunctionalProgramming {
	
	
	public static void main(String args[]){
		String[] myArray=new String[]{"bob", "alice", "paul", "ellie"};
		Stream<String > myStream=Arrays.stream(myArray);
		
		Stream<String> myNewStream=myStream.map(s->s.toUpperCase()).map(s->s+=" !");
		
//		myNewStream.forEach(s->{
//			s+=" !";
//			System.out.println(s);
////			return s;
//		});
		Stream<String> filteredStream=myNewStream.filter(s->s.length()>4);
		String result=filteredStream.reduce("",(s1,s2)->s1+" "+s2);
		System.out.println(result);
//		filteredStream.forEach(s->System.out.println(s));
	}
}
